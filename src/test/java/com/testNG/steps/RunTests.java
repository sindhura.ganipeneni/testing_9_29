package com.testNG.steps;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/Destination", "json:target/test-report.json"},
		features = "src/main/resources/features",
		monochrome = false)
public class RunTests {

}
