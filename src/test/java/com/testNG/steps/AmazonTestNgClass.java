package com.testNG.steps;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AmazonTestNgClass {
	WebDriver driver;
	
	@Parameters("browser")
	
	  @BeforeClass
	  public void beforeTest(String browser) {
		  if(browser.equals("firefox"))
		  {
		  System.setProperty("webdriver.gecko.driver","src/test/java/resources/geckodriver.exe");
			driver= new FirefoxDriver();
			}
		  else if(browser.equals("chrome"))
		  {
			  System.setProperty("webdriver.chrome.driver","src/test/java/resources/chromedriver.exe");
				driver = new ChromeDriver();
		  }
	/*	  else if(browser.equals("ie"))
		  {
			  System.setProperty("webdriver.ie.driver","src/test/java/resources/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
		  } 
		  //IE is not functional for me despite changing the security settings as you had instructed
		  */
			driver.get("https://www.amazon.com/");
	  }
  @Test(description="Enter 'echo' in text field")
  public void enterEchoInTextField() {

	  
	 WebElement search=driver.findElement(By.id("twotabsearchtextbox"));
	  search.sendKeys("echo dot");
	  /*WebElement clickSearch=driver.findElement(By.id("nav-search-submit-text"));
	  System.out.println(clickSearch);
	  clickSearch.click();  */
	  AssertJUnit.assertEquals(true, driver.getPageSource().contains("Echo Dot"));
  }

  @AfterTest
  public void afterTest() {
	 // driver.quit();
	  //driver.close();
	  
  }

}
